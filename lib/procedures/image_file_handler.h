#ifndef IMAGE_FILE_HANDLER_H
#define IMAGE_FILE_HANDLER_H

#include "../files/file_handler.h"
#include "../formats_common/format_functions.h"
#include "../formats_common/io_codes.h"
#include "../utils/utils.h"
#include "procedure_codes.h"


// wrapper function for opening a file & reading image from it 
// (technically abstracted from formats - image format is handled by read_image_func passed as an argument)
enum procedure_status read_image(const char* const file_name, read_image_func read_image_func, struct image* const result);

// wrapper function for opening a file & writing image to it
// (technically abstracted from formats - iamge format is handled by write_image_func passed as an argument)
enum procedure_status write_image(const char* const file_name, write_image_func write_image_func, const struct image image);

#endif
