#ifndef UTILS_H
#define UTILS_H

#include <stdio.h>

void print_message(const char* const message);
void println_message(const char* const message);
void print_error(const char* const message);
void println_error(const char* const message);

#endif
