#ifndef FILE_CODES_H
#define FILE_CODES_H

enum open_status {
    OPEN_OK = 0,
    OPEN_ERR
};

enum close_status {
    CLOSE_OK = 0,
    CLOSE_ERR
};

#endif
