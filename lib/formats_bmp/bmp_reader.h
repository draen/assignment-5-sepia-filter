#ifndef BMP_READER_H
#define BMP_READER_H

#include <stdio.h>

#include "../formats_common/io_codes.h"
#include "../img/image.h"
#include "bmp.h"

//function for reading an image from .bmp file
enum read_status bmp_read_image(FILE* const file, struct image* const result);

#endif
