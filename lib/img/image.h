#ifndef IMAGE_H
#define IMAGE_H

#include <malloc.h>
#include <stdint.h>

struct __attribute__((packed)) pixel {
    uint8_t b, g, r;
};

struct image {
    size_t width, height;
    struct pixel* pixels;
};

struct image alloc_image(size_t width, size_t height);

void free_image(struct image image);

struct pixel get_pixel(const struct image* const src, size_t row, size_t col);

void set_pixel(struct image* const dest, size_t row, size_t col, struct pixel pixel);

size_t get_size(const struct image image);

#endif
