#include <stdio.h>
#include <sys/time.h>
#include <sys/resource.h>

#include "../lib/procedures/image_file_handler.h"
#include "../lib/formats_bmp/bmp_reader.h"
#include "../lib/formats_bmp/bmp_writer.h"
#include "sepia/sepia.h"
#include "../lib/img/image.h"
#include "../lib/procedures/procedure_codes.h"
#include "../lib/utils/utils.h"


struct timeval get_time() {
    struct rusage r = {0};
    getrusage(RUSAGE_SELF, &r);
    return r.ru_utime;
}

void display_time_diff(struct timeval start, struct timeval finish) {
    long res = (finish.tv_sec - start.tv_sec) * 1000000L + finish.tv_usec - start.tv_usec;
    printf("Time taken: %ldus\n", res);
}

int main( int argc, char** argv ) {
    println_message("Image transformer started...");
    if (argc != 4) {
        println_error("Incorrect args provided; usage: `image-transformer <mode (c|asm)> <original image file> <transformed image file>`");
        return 0;
    }
    const char sepia_mode = *argv[1];
    if (sepia_mode!='a' && sepia_mode!='c') {
        println_error("Incorrect arg provided: the mode value should either be 'c' or 'asm'");
        return 0;
    }

    const char* const input_file_name = argv[2];
    const char* const output_file_name = argv[3];

    struct image initial_image = {0};
    enum procedure_status is_read_success = read_image(input_file_name, bmp_read_image, &initial_image);
    //if read wasn't successful exit (error message already displayed by read_image function)
    if (is_read_success != PROCEDURE_OK){
        free_image(initial_image);
        println_message("Image transformer encountered an error and finished working");
        return 0;
    } 

    print_message("Sepia filter starting in ");
    if (sepia_mode=='a') print_message("ASM");
    else print_message("C");
    println_message(" mode.");

    struct timeval start = get_time();
    const struct image new_image = sepia_mode=='a' ? sepia_asm(initial_image) : sepia(initial_image);
    free_image(initial_image); //don't need initial_image anymore
    struct timeval finish = get_time();

    display_time_diff(start, finish);

    enum procedure_status is_write_success = write_image(output_file_name, bmp_write_image, new_image);
    //if write wasn't successful exit (error message already displayed by write_image function)
    if (is_write_success != PROCEDURE_OK){ 
        free_image(new_image);
        println_message("Image transformer encountered an error and finished working");
        return 0;
    }

    free_image(new_image);

    println_message("Image transformer successfully finished working");
    return 0;
}
