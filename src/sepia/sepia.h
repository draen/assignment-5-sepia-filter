#ifndef SEPIA_H
#define SEPIA_H


#include "../../lib/img/image.h"

struct image sepia(struct image const src);

struct image sepia_asm(struct image const src);
#endif
