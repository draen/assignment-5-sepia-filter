
%macro clear_calc_regs 0
    ;macro for clearing xmm0, xmm1 & xmm2
    pxor xmm0,xmm0
    pxor xmm1,xmm1
    pxor xmm2,xmm2
%endmacro

%macro insert_data 1
    ; %1 - batch's number (1, 2 or 3)

    ; put pixels in xmm-registers
    ; each pixel contains 3 byte-sized uint-numbers
    ; after converting to floats each number becomes 4 byte-sized, so pixel will be 12 bytes
    ; xmm-registers are 16 bytes long

    ; basically we can support (xmm-length / num-size = 16 / 4 = 4) 4 operations at once
    ; but for each pixel we only need 3 operations (calc newB, newG and newR)
    ; so to achive maximum efficiency we will be calculating as follows:

    ; 1st batch: newB1, newG1, newR1, newB2
    ; 2nd batch: newG2, newR2, newB3, newG3
    ; 3rd batch: newR3, newB3, newG3, newR3
    ; then repeat

    pinsrb xmm0, [rdi],         0   ; b[i]   ->  xmm0[0]
    pinsrb xmm1, [rdi+1],       0   ; g[i]   ->  xmm1[0]
    pinsrb xmm2, [rdi+2],       0   ; r[i]   ->  xmm2[0]

    pinsrb xmm0, [rdi+3],       12  ; b[i+1] ->  xmm0[12]
    pinsrb xmm1, [rdi+3+1],     12  ; g[i+1] ->  xmm1[12]
    pinsrb xmm2, [rdi+3+2],     12  ; r[i+1] ->  xmm2[12]

    ; in order to support operations for 1st, 2nd & 3rd batches we need different "combinations" of bytes in xmm registers.
    ; For example - in 1st batch we need to do 3 operations for pixel#1 and 1 operation for pixel#2, so we will need 3 copies
    ; of pixel#1 bytes and 1 copy of pixel#2 bytes, so like this:
    ; xmm0:     B2  B1  B1  B1
    ; xmm1:     G2  G1  G1  G1
    ; xmm2:     R2  R1  R1  R1
    ; -------------------------
    ; calc:     B2' R1' G1' B1'
    ; that means we need a shuffle-pattern of 11_00_00_00 (for X2 X1 X1 X1)
    
    ; for 2nd batch:
    ; xmm0:     B3  B3  B2  B2
    ; xmm1:     G3  G3  G2  G2
    ; xmm2:     R3  R3  R2  R2
    ; -------------------------
    ; calc:     G3' B3' R2' G2'
    ; that means we need a shuffle-pattern of 11_11_00_00 (for X3 X3 X2 X2)

    ; for 3rd batch:
    ; xmm0:     B4  B4  B4  B3
    ; xmm1:     G4  G4  G4  G3
    ; xmm2:     R4  R4  R4  R3
    ; -------------------------
    ; calc:     R4' G4' B4' R3'
    ; that means we need a shuffle-pattern of 11_11_11_00 (for X4 X4 X4 X3)


    ; to handle shuffle-pattern choice we will define a rule below which uses the 1 argument of this macro
    %ifnum %1
        %if %1==1   
        ;1st batch
        %define shuffle 0b11000000 

        %elif %1==2
        ;2nd batch
        %define shuffle 0b11110000 

        %elif %1==3
        ;3rd batch
        %define shuffle 0b11111100

        %else
            %error "only numbers supported are 1, 2 & 3"
        %endif 
    %else
        %error "wanted a number"
    %endif

    ;applying the shuffle
    shufps xmm0,xmm0, shuffle
    shufps xmm1,xmm1, shuffle
    shufps xmm2,xmm2, shuffle
%endmacro

%macro apply_sepia 0
    ; performing calculations for sepia-filter

    ; convert values to floats
    cvtdq2ps xmm0, xmm0
    cvtdq2ps xmm1, xmm1
    cvtdq2ps xmm2, xmm2
    ; multiply by sepia-matrix
    mulps xmm0, xmm3
    mulps xmm1, xmm4
    mulps xmm2, xmm5
    ; calculate sum for final results
    addps xmm0, xmm1
    addps xmm0, xmm2
    ;convert back to ints
    cvtps2dq xmm0, xmm0

    ;do max-min stuff
    mov r8, max_sepia_values
    pminsd xmm0, [r8]
%endmacro

%macro extract_results 0
    ;macro for writing results of calculating a batch
    pextrb [rsi],   xmm0,   0
    pextrb [rsi+1], xmm0,   4
    pextrb [rsi+2], xmm0,   8
    pextrb [rsi+3], xmm0,   12
%endmacro


%macro handle_batch 1
    ; %1 - batch number
    ;wrapper-macro for all operations needed to handle 1 batch
    clear_calc_regs
    insert_data %1
    apply_sepia
    extract_results
%endmacro

%macro step 0
    ;macro for all operations needed to prepare for next batch

    ;moving data pointers before going for next batch
    add rdi, 3  ;go to next pixel (sizeof(pixel)=3)
    add rsi, 4  ;in each batch we calculate 4 values and save them, so we need to move pointer over them

    ;shuffle sepia matrix - explanation below
    sepia_matrix_shuffle
%endmacro

%macro insert_sepia_matrix 0
    mov r8, initial_sepia_matrix1
    movdqa xmm3, [r8]
    mov r8, initial_sepia_matrix2
    movdqa xmm4, [r8]
    mov r8, initial_sepia_matrix3
    movdqa xmm5, [r8]
%endmacro

; while working we will also need to shuffle the sepia matrix, because:
; 1st batch:    calc:     B2' R1' G1' B1'
; 2nd batch:    calc:     G3' B3' R2' G2'
; 3rd batch:    calc:     R4' G4' B4' R3'
; (see explanation in `insert_data` macro)
; simplified we get:
;       3   2   1   0 (column numbers for reference)
;   ------------------
;   1:  B   R   G   B
;   2:  G   B   R   G
;   3:  R   G   B   R
; it's not too hard to see that for each batch we need to do a replacement like this:
;       1   3   2   1
;   or, when written in binary: 01 11 10 01 
%define sepia_matrix_shuffle_pattern 0b01111001

%macro sepia_matrix_shuffle 0
    shufps xmm3,xmm3, sepia_matrix_shuffle_pattern
    shufps xmm4,xmm4, sepia_matrix_shuffle_pattern
    shufps xmm5,xmm5, sepia_matrix_shuffle_pattern
%endmacro

section .rodata
align 16
initial_sepia_matrix1:  dd 0.131,  0.168, 0.189, 0.131   ; Blue multipliers
align 16
initial_sepia_matrix2:  dd 0.534,  0.686, 0.769, 0.534   ; Green multipliers
align 16
initial_sepia_matrix3:  dd 0.272,  0.349, 0.393, 0.272   ; Red multipliers
align 16
max_sepia_values:       dd 0xFF, 0xFF, 0xFF, 0xFF   ; 


section .text
global sepia_asm_impl
sepia_asm_impl:
    ; rdi - pointer to initial data
    ; rsi - pointer to results

    ; initial data is like this: b1 g1 r1  b2 g2 r2  b3 g3 r3  b4 g4 r4
    insert_sepia_matrix

    handle_batch 1

    step
    handle_batch 2

    step
    handle_batch 3

    ret