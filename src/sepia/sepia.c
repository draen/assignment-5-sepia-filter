#include "sepia.h"

static uint8_t color(struct pixel pixel, double rMul, double gMul, double bMul) {
    double value = pixel.r * rMul + pixel.g * gMul + pixel.b * bMul;
    if (value > 255.0) return 255;
    return (uint8_t)value;
}

static struct pixel do_filter(struct pixel const src) {
    struct pixel new = {0};
    new.r = color(src, .393, .769, .189);
    new.g = color(src, .349, .686, .168);
    new.b = color(src, .272, .534, .131);
    return new;
}

struct image sepia(struct image const src) {
    struct image new = alloc_image(src.width, src.height);

    for (size_t row = 0; row < src.height; row++) {
        for (size_t col = 0; col < src.width; col++) {
            struct pixel original = get_pixel(&src, row, col);
            set_pixel(&new, row, col, do_filter(original));
        }
    }
    return new;
}

extern void sepia_asm_impl(struct pixel[static 4], struct pixel * restrict);
struct image sepia_asm(struct image const src) {
    struct image new = alloc_image(src.width, src.height);
    size_t amount = src.width * src.height;

    for (size_t i = 0; i < amount / 4; i++) {
        sepia_asm_impl(src.pixels+4*i, new.pixels+4*i);
    }

    for (size_t i = amount - amount % 4; i < amount; i++) {
        new.pixels[i] = do_filter(src.pixels[i]);
    }
    return new;
}